import React, { useState, useEffect } from 'react';
import { getPoke, getAllPoke } from './services/poke';
import Card from './CDD/Card';
import './App.css';

function App() {
  const [pokeData, setPokeData] = useState([])
  const [nextUrl, setNextUrl] = useState('');
  const [prevUrl, setPrevUrl] = useState('');
  const [loading, setLoading] = useState(true);
  const initialURL = 'https://pokeapi.co/api/v2/pokemon/'

  useEffect(() => {
    async function fetchData() {
      let response = await getAllPoke(initialURL)
      setNextUrl(response.next);
      setPrevUrl(response.previous);
      await loadingPoke(response.results);
      setLoading(false);
    }
    fetchData();
  }, [])


  const next = async () => {
    setLoading(true);
    let data = await getAllPoke(nextUrl)
    await loadingPoke(data.results)
    setNextUrl(data.next);
    setPrevUrl(data.previous);
    setLoading(false);
  }

  const prev = async () => {
    if (!prevUrl) {
      return;
    }
    setLoading(true);
    let data = await getAllPoke(prevUrl)
    await loadingPoke(data.results)
    setNextUrl(data.next);
    setPrevUrl(data.previous);
    setLoading(false);
  }

  const loadingPoke = async (data) => {
    let _pokeData = await Promise.all(data.map(async poke => {
      let pokeresource = await getPoke(poke)
      return pokeresource
    }))
    setPokeData(_pokeData);
  }

  // console.log(pokeData)

  return (
    <>

      <div>
        {loading ? <h1 style={{ textAlign: 'center' }}>...กำลังโหลดรอสักครู่...</h1> : (
          <>
            <div className="btn">
              <button onClick={prev}>กลับ</button>
              <button onClick={next}>ไปต่อ</button>
            </div>
            <div className="grid-container">
              {pokeData.map((poke, i) => {
                return <Card key={i} poke={poke} />
              })}
            </div>
            <div className="btn">
              <button onClick={prev}>กลับ</button>
              <button onClick={next}>ไปต่อ</button>
            </div>
          </>
        )}
      </div>
    </>
  );
            }

export default App;