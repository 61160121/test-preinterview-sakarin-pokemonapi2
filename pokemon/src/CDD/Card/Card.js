import React from 'react';
import './style.css';

function Card({ poke }) {
    return (
        <div className="Card">
            <div className="Card__img">
                <img src={poke.sprites.front_default} alt="" />
            </div>
            <div className="Card__name">
                <p>ชื่อ: </p> {poke.name}
            </div>

            <div className="Card__types"><p className="type">ประเภท</p> </div>
            
            <div className="Card__types">
                {
                    poke.types.map(type => {
                        return (
                            <div className="Card__type" >
                                {type.type.name}
                            </div>
                        )
                    })
                }
            </div>
            <div className="Card__info">
                <div className="Card__data Card__data--weight">
                    <p className="title">น้ำหนัก</p>
                    <p>{poke.weight}</p>
                </div>
                <div className="Card__data Card__data--weight">
                    <p className="title">สูง</p>
                    <p>{poke.height}</p>
                </div>
                <div className="Card__data Card__data--ability">
                    <p className="title">ความสามารถ</p>
                    <p>{poke.abilities[0].ability.name}</p>
                </div>
            </div>
        </div>
    );
}

export default Card;